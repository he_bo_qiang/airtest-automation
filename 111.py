# -*- encoding=utf8 -*-
from poco.exceptions import PocoTargetTimeout

__author__ = "hvnb"

from airtest.core.api import *

auto_setup(__file__)


from poco.drivers.android.uiautomation import AndroidUiautomationPoco
poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)

# pip install -i https://pypi.tuna.tsinghua.edu.cn/simple/ 库名
# 点击
poco(desc = "驾考宝典").click()
# double_click((399,859))
# # double_click((399,859))
# poco(desc = "驾考宝典",type = "android.widget.ImageView").click()
# poco(desc = "驾考宝典",type = "android.widget.ImageView").click()
# poco(text="道长 ，今晚还是你上课吗").long_click(duration = 5)
# poco(text="昨天视频没提呢吧").long_click(duration = 5)
# #输入
# poco("com.handsgo.jiakao.android:id/edt_search_q").set_text("慧测-道长")
# poco("com.handsgo.jiakao.android:id/edt_search_q").set_text("何卜强")
#
# # 对象存在判断
# invisible_obj = poco(desc = "驾考宝典")
# print(invisible_obj.exists())
# obj = poco("驾考宝典1")
# print(obj.exists())
#
# #拖动和滑动（drag/swipe）
# poco("com.handsgo.jiakao.android:id/clRemote").drag_to(poco("com.handsgo.jiakao.android:id/iv_icon"))
# poco("com.handsgo.jiakao.android:id/clRemote").drag_to(poco(text="科一"))
# #选中UI执行swipe
# poco("com.handsgo.jiakao.android:id/clRemote").swipe([0,-0.1])
# poco("com.handsgo.jiakao.android:id/clRemote").swipe([+0.1,+0.1])
# # poco("com.handsgo.jiakao.android:id/clRemote").swipe("up")
# poco("com.handsgo.jiakao.android:id/clRemote").swipe("up")
# poco("com.handsgo.jiakao.android:id/clRemote").swipe("down")
# poco("com.handsgo.jiakao.android:id/clRemote").swipe("left")
# poco("com.handsgo.jiakao.android:id/clRemote").swipe("right")
# #不选中UI执行swipe
# x,y = poco("com.handsgo.jiakao.android:id/clRemote").get_position()
x,y = poco("com.handsgo.jiakao.android:id/clRemote").get_position()
end =[x,y +0.3]
#从A点滑到B点
poco.swipe([x,y],end)
#等待元素出现或消失
poco("驾考宝典").wait_for_appearance(timeout =10)

try:
    poco("驾考宝典").wait_for_disappearance(timeout =5)
except PocoTargetTimeout:
    snapshot(msg = "元素出现或未出现")
poco("驾考宝典").wait_for_appearance(timeout = 10)
poco("驾考宝典").wait_for_disappearance(timeout =10)
#轮询UI等待,任意一个UI出现就往下走
jiakao =poco("驾考宝典")
dingding=poco("钉钉 1 个未读")
meituan=poco("美团")
poco.wait_for_any([jiakao,dingding,meituan])
poco("驾考宝典").click()

#轮询UI时等待，所有UI出现才往下走
dingding=poco("钉钉 2 个未读")
meituan=poco("美团")
poco.wait_for_all([dingding,meituan])
poco("QQ").click()
#打开app
start_app("com.handsgo.jiakao.android")
start_app("com.handsgo.jiakao.android")
#关闭app
stop_app("com.handsgo.jiakao.android")
#
value = poco(name = "驾考宝典").attr("name")
print(value)
try:
    assert_equal(value,"驾考宝典1","是否为驾考宝典")
except AssertionError:
    print("驾考宝典断言失败")


