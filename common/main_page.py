#通过读取config的配置，使用connect_device函数连接手机，使用start_app函数启动app，其中MainPage作为PO设计模式里的基页，承担了初始化poco的功能
from poco.drivers.android.uiautomation import AndroidUiautomationPoco
class MainPage:
    poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)
 